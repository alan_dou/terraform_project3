terraform {
  backend "azurerm" {
    resource_group_name  = "rg-activity2"
    storage_account_name = "saactivity2dou"
    container_name       = "bcactivity2dou"
    key                  = "terraform.tfstate"
  }
  required_providers {
    azurerm = "~> 2.0"
  }
}


provider "azurerm" {
  features {}
}

data "azurerm_resource_group" "my-rg" {
  name = "rg-activity2"
}


resource "azurerm_virtual_network" "vn-project-1" {
  name                = "vn-proyect2-dou"
  address_space       = ["10.0.0.0/16"]
  location            = data.azurerm_resource_group.my-rg.location                   
  resource_group_name = data.azurerm_resource_group.my-rg.name
}

resource "azurerm_network_security_group" "myterraformnsg" {
    name                = "myNetworkSecurityGroup"
    location            = "eastus"
    resource_group_name = data.azurerm_resource_group.my-rg.name

    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    tags = {
        environment = "Terraform Demo"
    }
}